import ballerina/http;
import ballerina/time;
import ballerina/runtime;

import wso2/gateway;









     
http:Client get62e98ec452d7408c84ea647d287b0be2_prod = new (
gateway:retrieveConfig("d9519e9100bf4185b4a9d6808623fccd_prod_endpoint_0","http://localhost:80/api/v1"),
{ 
httpVersion: gateway:getClientsHttpVersion()
,
    cache: { enabled: false }


,
secureSocket: gateway:getClientSecureSocket()

,
    poolConfig: gateway:getClientPoolConfig(true, 0, -1, -1, -1)


, http1Settings : {
    proxy: gateway:getClientProxyConfig()
}


});
 
    
    
    
    
    

    
    

    
    


     
http:Client getda26f4bedc844ce599300138ccebe55a_prod = new (
gateway:retrieveConfig("da267120425e4101a4643274cefe4c45_prod_endpoint_0","http://localhost:9080"),
{ 
httpVersion: gateway:getClientsHttpVersion()
,
    cache: { enabled: false }


,
secureSocket: gateway:getClientSecureSocket()

,
    poolConfig: gateway:getClientPoolConfig(true, 0, -1, -1, -1)


, http1Settings : {
    proxy: gateway:getClientProxyConfig()
}


});
 
    
    
    
    
    

    
    

    
    


     
http:Client get9fd981bfe3a549d49af97977af427bff_prod = new (
gateway:retrieveConfig("bebf69b432b24ea6ae7fa723f7523249_prod_endpoint_0","http://localhost:9083"),
{ 
httpVersion: gateway:getClientsHttpVersion()
,
    cache: { enabled: false }


,
secureSocket: gateway:getClientSecureSocket()

,
    poolConfig: gateway:getClientPoolConfig(true, 0, -1, -1, -1)


, http1Settings : {
    proxy: gateway:getClientProxyConfig()
}


});
 
    
    
    
    
    

    
    

    
    



//This variable is added for logging purposes
string Cardealer_space_API__1_0_0Key = "Cardealer API-1.0.0";









@http:ServiceConfig {
    basePath: "/api/v1",
    auth: {
        authHandlers: gateway:getAuthHandlers(["apikey"], false, false)
    }
   
}

@gateway:API {
    publisher:"",
    name:"Cardealer API",
    apiVersion: "1.0.0",
    apiTier : "" ,
    authProviders: ["apikey"],
    security: {
            "apikey":[ { "in": "header", "name": "X-API-KEY" } ],
            "mutualSSL": "",
            "applicationSecurityOptional": false
        }
}
service Cardealer_space_API__1_0_0 on apiListener,
apiSecureListener {


    @http:ResourceConfig {
        methods:["GET"],
        path:"/cardealers",
        auth:{
        
            
        
            authHandlers: gateway:getAuthHandlers(["apikey"], false, false)
        }
    }
    @gateway:Resource {
        authProviders: ["apikey"],
        security: {
            "apikey":[],
            "applicationSecurityOptional": false 
            }
    }
    @gateway:RateLimit{policy : ""}
    resource function get62e98ec452d7408c84ea647d287b0be2 (http:Caller outboundEp, http:Request req
) {
        handleExpectHeaderForCardealer_space_API__1_0_0(outboundEp, req);
        runtime:InvocationContext invocationContext = runtime:getInvocationContext();

        map<string> pathParams = { 
        };
        invocationContext.attributes["pathParams"] = pathParams;
        

        
        string urlPostfix = gateway:replaceFirst(req.rawPath,"/api/v1","");
        
        if(urlPostfix != "" && !gateway:hasPrefix(urlPostfix, "/")) {
            urlPostfix = "/" + urlPostfix;
        }
        http:Response|error clientResponse;
        http:Response r = new;
        clientResponse = r;
        string destination_attribute;
        invocationContext.attributes["timeStampRequestOut"] = time:currentTime().time;
        boolean reinitRequired = false;
        string failedEtcdKey = "";
        string failedEtcdKeyConfigValue = "";
        boolean|error hasUrlChanged;
        http:ClientConfiguration newConfig;
        boolean reinitFailed = false;
        boolean isProdEtcdEnabled = false;
        boolean isSandEtcdEnabled = false;
        map<string> endpointEtcdConfigValues = {};
        
        
            
            
                
                    
                    if("PRODUCTION" == <string>invocationContext.attributes["KEY_TYPE"]) {
                    
                
                
                    
    clientResponse = get62e98ec452d7408c84ea647d287b0be2_prod->forward(urlPostfix, <@untainted>req);

invocationContext.attributes["destination"] = "http://localhost:80/api/v1";
                    
                        } else {
                            http:Response res = new;
res.statusCode = 403;
string errorMessage = "Sandbox key offered to the API with no sandbox endpoint";
if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
    json payload = {
        ERROR_CODE: "900901",
        ERROR_MESSAGE: errorMessage
    };
    res.setPayload(payload);
} else {
    gateway:attachGrpcErrorHeaders (res, errorMessage);
}
invocationContext.attributes["error_code"] = "900901";
clientResponse = res;
                    
                    }
                
            
        
        invocationContext.attributes["timeStampResponseIn"] = time:currentTime().time;


        if(clientResponse is http:Response) {
            

            invocationContext.attributes[gateway:DID_EP_RESPOND] = true;
            var outboundResult = outboundEp->respond(clientResponse);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        } else {
            http:Response res = new;
            res.statusCode = 500;
            string errorMessage = clientResponse.reason();
            int errorCode = 101503;
            string errorDescription = "Error connecting to the back end";

            if(gateway:contains(errorMessage, "connection timed out") || gateway:contains(errorMessage,"Idle timeout triggered")) {
                errorCode = 101504;
                errorDescription = "Connection timed out";
            }
            if(gateway:contains(errorMessage, "Malformed URL")) {
                errorCode = 101505;
                errorDescription = "Malformed URL";
            }
            invocationContext.attributes["error_response_code"] = errorCode;
            invocationContext.attributes["error_response"] = errorDescription;
            if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
                json payload = {fault : {
                                code : errorCode,
                                message : "Runtime Error",
                                description : errorDescription
                            }};

                            res.setPayload(payload);
            } else {
                gateway:attachGrpcErrorHeaders (res, errorDescription);
            }
            gateway:printError(Cardealer_space_API__1_0_0Key, "Error in client response", clientResponse);
            var outboundResult = outboundEp->respond(res);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        }
    }


    @http:ResourceConfig {
        methods:["GET"],
        path:"/details/{id}",
        auth:{
        
            
        
            authHandlers: gateway:getAuthHandlers(["apikey"], false, false)
        }
    }
    @gateway:Resource {
        authProviders: ["apikey"],
        security: {
            "apikey":[],
            "applicationSecurityOptional": false 
            }
    }
    @gateway:RateLimit{policy : ""}
    resource function getda26f4bedc844ce599300138ccebe55a (http:Caller outboundEp, http:Request req, string id 
) {
        handleExpectHeaderForCardealer_space_API__1_0_0(outboundEp, req);
        runtime:InvocationContext invocationContext = runtime:getInvocationContext();

        map<string> pathParams = { 
            "id": <@untainted>id 
        };
        invocationContext.attributes["pathParams"] = pathParams;
        

        
        string urlPostfix = gateway:replaceFirst(req.rawPath,"/api/v1","");
        
        if(urlPostfix != "" && !gateway:hasPrefix(urlPostfix, "/")) {
            urlPostfix = "/" + urlPostfix;
        }
        http:Response|error clientResponse;
        http:Response r = new;
        clientResponse = r;
        string destination_attribute;
        invocationContext.attributes["timeStampRequestOut"] = time:currentTime().time;
        boolean reinitRequired = false;
        string failedEtcdKey = "";
        string failedEtcdKeyConfigValue = "";
        boolean|error hasUrlChanged;
        http:ClientConfiguration newConfig;
        boolean reinitFailed = false;
        boolean isProdEtcdEnabled = false;
        boolean isSandEtcdEnabled = false;
        map<string> endpointEtcdConfigValues = {};
        
        
            
            
                
                    
                    if("PRODUCTION" == <string>invocationContext.attributes["KEY_TYPE"]) {
                    
                
                
                    
    clientResponse = getda26f4bedc844ce599300138ccebe55a_prod->forward(urlPostfix, <@untainted>req);

invocationContext.attributes["destination"] = "http://localhost:9080";
                    
                        } else {
                            http:Response res = new;
res.statusCode = 403;
string errorMessage = "Sandbox key offered to the API with no sandbox endpoint";
if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
    json payload = {
        ERROR_CODE: "900901",
        ERROR_MESSAGE: errorMessage
    };
    res.setPayload(payload);
} else {
    gateway:attachGrpcErrorHeaders (res, errorMessage);
}
invocationContext.attributes["error_code"] = "900901";
clientResponse = res;
                    
                    }
                
            
        
        invocationContext.attributes["timeStampResponseIn"] = time:currentTime().time;


        if(clientResponse is http:Response) {
            

            invocationContext.attributes[gateway:DID_EP_RESPOND] = true;
            var outboundResult = outboundEp->respond(clientResponse);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        } else {
            http:Response res = new;
            res.statusCode = 500;
            string errorMessage = clientResponse.reason();
            int errorCode = 101503;
            string errorDescription = "Error connecting to the back end";

            if(gateway:contains(errorMessage, "connection timed out") || gateway:contains(errorMessage,"Idle timeout triggered")) {
                errorCode = 101504;
                errorDescription = "Connection timed out";
            }
            if(gateway:contains(errorMessage, "Malformed URL")) {
                errorCode = 101505;
                errorDescription = "Malformed URL";
            }
            invocationContext.attributes["error_response_code"] = errorCode;
            invocationContext.attributes["error_response"] = errorDescription;
            if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
                json payload = {fault : {
                                code : errorCode,
                                message : "Runtime Error",
                                description : errorDescription
                            }};

                            res.setPayload(payload);
            } else {
                gateway:attachGrpcErrorHeaders (res, errorDescription);
            }
            gateway:printError(Cardealer_space_API__1_0_0Key, "Error in client response", clientResponse);
            var outboundResult = outboundEp->respond(res);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        }
    }


    @http:ResourceConfig {
        methods:["GET"],
        path:"/reviews/{id}",
        auth:{
        
            
        
            authHandlers: gateway:getAuthHandlers(["apikey"], false, false)
        }
    }
    @gateway:Resource {
        authProviders: ["apikey"],
        security: {
            "apikey":[],
            "applicationSecurityOptional": false 
            }
    }
    @gateway:RateLimit{policy : ""}
    resource function get9fd981bfe3a549d49af97977af427bff (http:Caller outboundEp, http:Request req, string id 
) {
        handleExpectHeaderForCardealer_space_API__1_0_0(outboundEp, req);
        runtime:InvocationContext invocationContext = runtime:getInvocationContext();

        map<string> pathParams = { 
            "id": <@untainted>id 
        };
        invocationContext.attributes["pathParams"] = pathParams;
        

        
        string urlPostfix = gateway:replaceFirst(req.rawPath,"/api/v1","");
        
        if(urlPostfix != "" && !gateway:hasPrefix(urlPostfix, "/")) {
            urlPostfix = "/" + urlPostfix;
        }
        http:Response|error clientResponse;
        http:Response r = new;
        clientResponse = r;
        string destination_attribute;
        invocationContext.attributes["timeStampRequestOut"] = time:currentTime().time;
        boolean reinitRequired = false;
        string failedEtcdKey = "";
        string failedEtcdKeyConfigValue = "";
        boolean|error hasUrlChanged;
        http:ClientConfiguration newConfig;
        boolean reinitFailed = false;
        boolean isProdEtcdEnabled = false;
        boolean isSandEtcdEnabled = false;
        map<string> endpointEtcdConfigValues = {};
        
        
            
            
                
                    
                    if("PRODUCTION" == <string>invocationContext.attributes["KEY_TYPE"]) {
                    
                
                
                    
    clientResponse = get9fd981bfe3a549d49af97977af427bff_prod->forward(urlPostfix, <@untainted>req);

invocationContext.attributes["destination"] = "http://localhost:9083";
                    
                        } else {
                            http:Response res = new;
res.statusCode = 403;
string errorMessage = "Sandbox key offered to the API with no sandbox endpoint";
if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
    json payload = {
        ERROR_CODE: "900901",
        ERROR_MESSAGE: errorMessage
    };
    res.setPayload(payload);
} else {
    gateway:attachGrpcErrorHeaders (res, errorMessage);
}
invocationContext.attributes["error_code"] = "900901";
clientResponse = res;
                    
                    }
                
            
        
        invocationContext.attributes["timeStampResponseIn"] = time:currentTime().time;


        if(clientResponse is http:Response) {
            

            invocationContext.attributes[gateway:DID_EP_RESPOND] = true;
            var outboundResult = outboundEp->respond(clientResponse);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        } else {
            http:Response res = new;
            res.statusCode = 500;
            string errorMessage = clientResponse.reason();
            int errorCode = 101503;
            string errorDescription = "Error connecting to the back end";

            if(gateway:contains(errorMessage, "connection timed out") || gateway:contains(errorMessage,"Idle timeout triggered")) {
                errorCode = 101504;
                errorDescription = "Connection timed out";
            }
            if(gateway:contains(errorMessage, "Malformed URL")) {
                errorCode = 101505;
                errorDescription = "Malformed URL";
            }
            invocationContext.attributes["error_response_code"] = errorCode;
            invocationContext.attributes["error_response"] = errorDescription;
            if (! invocationContext.attributes.hasKey(gateway:IS_GRPC)) {
                json payload = {fault : {
                                code : errorCode,
                                message : "Runtime Error",
                                description : errorDescription
                            }};

                            res.setPayload(payload);
            } else {
                gateway:attachGrpcErrorHeaders (res, errorDescription);
            }
            gateway:printError(Cardealer_space_API__1_0_0Key, "Error in client response", clientResponse);
            var outboundResult = outboundEp->respond(res);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response", outboundResult);
            }
        }
    }

}

    function handleExpectHeaderForCardealer_space_API__1_0_0 (http:Caller outboundEp, http:Request req ) {
        if (req.expects100Continue()) {
            req.removeHeader("Expect");
            var result = outboundEp->continue();
            if (result is error) {
            gateway:printError(Cardealer_space_API__1_0_0Key, "Error while sending 100 continue response", result);
            }
        }
    }

function getUrlOfEtcdKeyForReInitCardealer_space_API__1_0_0(string defaultUrlRef,string etcdRef, string defaultUrl, string etcdKey) returns string {
    string retrievedEtcdKey = <string> gateway:retrieveConfig(etcdRef,etcdKey);
    map<any> urlChangedMap = gateway:getUrlChangedMap();
    urlChangedMap[<string> retrievedEtcdKey] = false;
    map<string> etcdUrls = gateway:getEtcdUrlsMap();
    string url = <string> etcdUrls[retrievedEtcdKey];
    if (url == "") {
        return <string> gateway:retrieveConfig(defaultUrlRef, defaultUrl);
    } else {
        return url;
    }
}

function respondFromJavaInterceptorCardealer_space_API__1_0_0(runtime:InvocationContext invocationContext, http:Caller outboundEp) returns boolean {
    boolean tryRespond = false;
    if(invocationContext.attributes.hasKey(gateway:RESPOND_DONE) && invocationContext.attributes.hasKey(gateway:RESPONSE_OBJECT)) {
        if(<boolean>invocationContext.attributes[gateway:RESPOND_DONE]) {
            http:Response clientResponse = <http:Response>invocationContext.attributes[gateway:RESPONSE_OBJECT];
            var outboundResult = outboundEp->respond(clientResponse);
            if (outboundResult is error) {
                gateway:printError(Cardealer_space_API__1_0_0Key, "Error when sending response from the interceptor", outboundResult);
            }
            tryRespond = true;
        }
    }
    return tryRespond;
}

function initInterceptorIndexesCardealer_space_API__1_0_0() {


    
        

        
        


    
        

        
        


    
        

        
        


}