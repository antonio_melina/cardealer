import wso2/gateway;
import ballerina/lang.'object;

const string MAIN_MUSTACHE = "Main";
public function main() {
    gateway:initNativeImpLog4jConfig();
    int totalResourceLength = 0;
    string projectName = "";
    boolean isRequestValidationEnabled  = gateway:getConfigBooleanValue(gateway:VALIDATION_CONFIG_INSTANCE_ID,
    gateway:REQUEST_VALIDATION_ENABLED, gateway:DEFAULT_REQUEST_VALIDATION_ENABLED);
    boolean isResponseValidationEnabled  = gateway:getConfigBooleanValue(gateway:VALIDATION_CONFIG_INSTANCE_ID,
    gateway:RESPONSE_VALIDATION_ENABLED, gateway:DEFAULT_RESPONSE_VALIDATION_ENABLED);
    
    string[] Cardealer_space_API__1_0_0_service = [ "get62e98ec452d7408c84ea647d287b0be2"
                                , "getda26f4bedc844ce599300138ccebe55a"
                                , "get9fd981bfe3a549d49af97977af427bff"
                                ];
    totalResourceLength = totalResourceLength +  Cardealer_space_API__1_0_0_service.length();
    gateway:populateAnnotationMaps("Cardealer_space_API__1_0_0", Cardealer_space_API__1_0_0, Cardealer_space_API__1_0_0_service);
    projectName = "cartest";
    
    error? err = gateway:extractJAR(projectName);
    if (err is error) {
        gateway:printError(gateway:MAIN, "Error when retrieving the resources artifacts", err);
    }
    gateway:initiateInterceptorArray(totalResourceLength);
    
    initInterceptorIndexesCardealer_space_API__1_0_0();
    
    addTokenServicesFilterAnnotation();
    gateway:startObservabilityListener();

        map<string> receivedRevokedTokenMap = gateway:getRevokedTokenMap();
    boolean jmsListenerStarted = gateway:initiateTokenRevocationJmsListener();

    string persistType = gateway:getConfigValue(gateway:PERSISTENT_MESSAGE_INSTANCE_ID,
        gateway:PERSISTENT_MESSAGE_TYPE, gateway:DEFAULT_PERSISTENT_TYPE);
    boolean useDefault = gateway:getConfigBooleanValue(gateway:PERSISTENT_MESSAGE_INSTANCE_ID,
            gateway:PERSISTENT_USE_DEFAULT, gateway:DEFAULT_PERSISTENT_USE_DEFAULT);

    if (persistType == "default") {
        future<()> initJwtRetriveal = start gateway:registerRevokedJwtRetrievalTask();
    } else if (persistType == "etcd" || useDefault ) {
        future<()> initETCDRetriveal = start gateway:etcdRevokedTokenRetrieverTask();
    } else {
        initiatePersistentRevokedTokenRetrieval(receivedRevokedTokenMap);
    }

    startupExtension();

    future<()> callhome = start gateway:invokeCallHome();
}

listener ObjectName jmsListener = new(); // This will be your normal listeners like http listner...
type ObjectName object {
    *'object:Listener;
    public function __attach(service s, string? name) returns error? {
    }
    public function __detach(service s) returns error? {
    }
    public function __start() returns error? {
        gateway:printDebug(MAIN_MUSTACHE, "Initializing throttling policies");
        initThrottlePolicies();
        gateway:printDebug(MAIN_MUSTACHE, "Initializing Notification JMS Listener");
        _ = start gateway:initiateNotificationJmsListener();
        gateway:printDebug(MAIN_MUSTACHE, "Initializing Throttle Data Publisher");
        gateway:initThrottleDataPublisher();
        gateway:printDebug(MAIN_MUSTACHE, "Initializing Global Throttle Data Publisher");
        gateway:initGlobalThrottleDataPublisher();
    }
    public function __gracefulStop() returns error? {
    }
    public function __immediateStop() returns error? {
    }
};

