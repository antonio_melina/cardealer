package com.tesi.review.controller;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReviewsController {

	// HTTP headers to propagate for distributed tracing are documented at
	// https://istio.io/docs/tasks/telemetry/distributed-tracing/overview/#trace-context-propagation
	private final static String[] headers_to_propagate = {
			// All applications should propagate x-request-id. This header is
			// included in access log statements and is used for consistent trace
			// sampling and log sampling decisions in Istio.
			"x-request-id",

			// Lightstep tracing header. Propagate this if you use lightstep tracing
			// in Istio (see
			// https://istio.io/latest/docs/tasks/observability/distributed-tracing/lightstep/)
			// Note: this should probably be changed to use B3 or W3C TRACE_CONTEXT.
			// Lightstep recommends using B3 or TRACE_CONTEXT and most application
			// libraries from lightstep do not support x-ot-span-context.
			"x-ot-span-context",

			// Datadog tracing header. Propagate these headers if you use Datadog
			// tracing.
			"x-datadog-trace-id", "x-datadog-parent-id", "x-datadog-sampling-priority",

			// W3C Trace Context. Compatible with OpenCensusAgent and Stackdriver Istio
			// configurations.
			"traceparent", "tracestate",

			// Cloud trace context. Compatible with OpenCensusAgent and Stackdriver Istio
			// configurations.
			"x-cloud-trace-context",

			// Grpc binary trace context. Compatible with OpenCensusAgent nad
			// Stackdriver Istio configurations.
			"grpc-trace-bin",

			// b3 trace headers. Compatible with Zipkin, OpenCensusAgent, and
			// Stackdriver Istio configurations. Commented out since they are
			// propagated by the OpenTracing tracer above.
			"x-b3-traceid", "x-b3-spanid", "x-b3-parentspanid", "x-b3-sampled", "x-b3-flags",

			// Application-specific headers to forward.
			"end-user", "user-agent", };

	private String getJsonResponse(String productId) {
		String result = "{";
		result += "\"id\": \"" + productId + "\",";
		result += "\"reviews\": [";

		// reviewer 1:
		result += "{";
		result += "  \"reviewer\": \"Reviewer1\",";
		result += "  \"text\": \"Guaranteed professionalism. Excellent after-sales assistance!\"";

		result += "},";

		// reviewer 2:
		result += "{";
		result += "  \"reviewer\": \"Reviewer2\",";
		result += "  \"text\": \"Recommended. very professional.\"";

		result += "},";

		// reviewer 3:
		result += "{";
		result += "  \"reviewer\": \"Reviewer3\",";
		result += "  \"text\": \"Car serviced after a month ... yuck.\"";

		result += "},";

		// reviewer 4:
		result += "{";
		result += "  \"reviewer\": \"Reviewer4\",";
		result += "  \"text\": \"I'll be back for my daughter's car. Great shop!\"";

		result += "},";

		// reviewer 5:
		result += "{";
		result += "  \"reviewer\": \"Reviewer5\",";
		result += "  \"text\": \"Unclear in telephone communications regarding the vehicles they sell.\"";

		result += "},";

		// reviewer 6:
		result += "{";
		result += "  \"reviewer\": \"Reviewer6\",";
		result += "  \"text\": \"It could be better.\"";

		result += "},";

		// reviewer 7:
		result += "{";
		result += "  \"reviewer\": \"Reviewer7\",";
		result += "  \"text\": \"Delay in the delivery of the car ... but not bad!\"";

		result += "}";

		result += "]";
		result += "}";

		return result;
	}

	@GetMapping("/health")
	public Response health() {
		return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\"status\": \"Reviews is healthy\"}").build();
	}

//    @GetMapping("/reviews/{productId}")
//    public Response bookReviewsById(@PathVariable("productId") int productId, @Context HttpHeaders requestHeaders) {
//
//      String jsonResStr = getJsonResponse(Integer.toString(productId));
//      return Response.ok().type(MediaType.APPLICATION_JSON).entity(jsonResStr).build();
//    }

	@GetMapping("/reviews/{productId}")
	public String bookReviewsById(@PathVariable("productId") int productId, @Context HttpHeaders requestHeaders) {

		String jsonResStr = getJsonResponse(Integer.toString(productId));
		// return
		// Response.ok().type(MediaType.APPLICATION_JSON).entity(jsonResStr).build();
		return jsonResStr;
	}

}
